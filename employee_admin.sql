CREATE OR REPLACE PACKAGE employee_admin AS
   invalid_payrise EXCEPTION;
   PROCEDURE create_employee ( 
				p_employee_name     VARCHAR2,
				p_job_title        	VARCHAR2,
				p_manager_id      	NUMBER,
				p_date_hired		DATE,
				p_salary			NUMBER,
				p_department_id		NUMBER);
   PROCEDURE salary_change (
				p_employee_id 		NUMBER, 
				p_percentage 		NUMBER);
   PROCEDURE transfer_employee (
				p_employee_id 		NUMBER,
				p_department_id		NUMBER);
   FUNCTION salary_enquiry (
				p_employee_id 		NUMBER) 
				RETURN NUMBER;	
END employee_admin;
/
CREATE OR REPLACE PACKAGE BODY employee_admin AS
   	PROCEDURE create_employee (
				p_employee_name     VARCHAR2,
				p_job_title        	VARCHAR2,
				p_manager_id      	NUMBER,
				p_date_hired		DATE,
				p_salary			NUMBER,
				p_department_id		NUMBER)
   	IS
	BEGIN
       INSERT INTO demo.employees 
			(employee_name,
			job_title,
			manager_id,
			date_hired,
			salary,
			department_id) 
		VALUES 
			(p_employee_name,
			p_job_title,
			p_manager_id,
			p_date_hired,
			p_salary,
			p_department_id);
		COMMIT;
    END create_employee;
	
	PROCEDURE salary_change (
				p_employee_id 		NUMBER, 
				p_percentage 		NUMBER)
   	IS
   	BEGIN
       IF p_percentage > 20 THEN
       		RAISE invalid_payrise;
       ELSE
           	UPDATE demo.employees SET salary = salary * (1 + (p_percentage / 100)) WHERE employee_id = p_employee_id;
            COMMIT;
       END IF;
    EXCEPTION
        WHEN invalid_payrise THEN
        	dbms_output.put_line('Pay rise is too generous. No changes made');
	END salary_change;
	
	PROCEDURE transfer_employee (
				p_employee_id 		NUMBER,
				p_department_id		NUMBER)
	IS
    BEGIN
        UPDATE demo.employees SET department_id = p_department_id WHERE employee_id = p_employee_id;
        COMMIT;
    END transfer_employee;
	
	FUNCTION salary_enquiry (
			p_employee_id 		NUMBER) 
			RETURN NUMBER 
	IS 
			v_salary NUMBER;
	BEGIN
    	SELECT salary INTO v_salary FROM demo.employees WHERE employee_id = p_employee_id;
    	RETURN v_salary;
	END salary_enquiry;
END employee_admin;
