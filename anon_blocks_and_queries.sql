-- Question 3

DECLARE
	v_employee_name varchar2(20) := 'Dennis Moore';
	v_job_title varchar2(20) := 'Salesperson';
	v_manager_id number := 90003;
	v_date_hired date := '1-Aug-2020';
	v_salary number := 19000;
	v_department_id number := 5;
BEGIN
	employee_admin.create_employee(v_employee_name,v_job_title,v_manager_id,v_date_hired,v_salary,v_department_id);
    COMMIT;
END;
/

-- Question 4

DECLARE
	v_percent number := 21;
	v_emp_id number := 90018;
BEGIN
	employee_admin.salary_change(v_emp_id,v_percent);
    COMMIT;
END;
/


-- Question 5

DECLARE
	v_emp_id number := 90006;
	v_department_id number := 1;
BEGIN
	employee_admin.transfer_employee(v_emp_id,v_department_id);
    COMMIT;
END;
/


-- Question 6

DECLARE
	v_emp_id number := 90006;
	v_salary number;
BEGIN
	v_salary := employee_admin.salary_enquiry(v_emp_id);
    dbms_output.put_line('The salary for employee_id ' || v_emp_id || ' is ' || v_salary);
    COMMIT;
END;
/


-- Question 7

SELECT 	
		d.department_name as department_name,
		e.employee_name as employee_name,
		e.job_title as job_title
from 
		demo.employees e, demo.department d 
WHERE 
		e.department_id = d.department_id
order by
		d.department_name

-- Question 8

SELECT 
		d.department_name as Department_Name,
		SUM(e.salary) as Total_Salary
from 
		demo.employees e, 
		demo.department d 
WHERE 
		e.department_id = d.department_id
group by
		d.department_name
order by 
		d.department_name
