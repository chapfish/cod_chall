SET DEFINE OFF;

INSERT INTO demo.department (department_id,department_name,location) VALUES (1, 'Management', 'London')
INSERT INTO demo.department (department_id,department_name,location) VALUES (2, 'Engineering', 'Cardiff');
INSERT INTO demo.department (department_id,department_name,location) VALUES (3, 'Research & Development', 'Edinburgh');
INSERT INTO demo.department (department_id,department_name,location) VALUES (4, 'Sales', 'Belfast');
COMMIT;

INSERT INTO demo.employees (employee_name,job_title,manager_id,date_hired,salary,department_id) VALUES ('John Smith', 'CEO',NULL,to_date('01/01/95', 'DD/MM/YY'),100000,1);
INSERT INTO demo.employees (employee_name,job_title,manager_id,date_hired,salary,department_id) VALUES ('Jimmy Willis', 'Manager', 90001, to_date('23/09/03', 'DD/MM/YY'), 52500, 4);
INSERT INTO demo.employees (employee_name,job_title,manager_id,date_hired,salary,department_id) VALUES ('Roxy Jones', 'Salesperson', 90002, to_date('11/02/17', 'DD/MM/YY'), 35000, 4);
INSERT INTO demo.employees (employee_name,job_title,manager_id,date_hired,salary,department_id) VALUES ('Selwyn Field', 'Salesperson', 90003, to_date('20/05/15', 'DD/MM/YY'), 32000, 4);
INSERT INTO demo.employees (employee_name,job_title,manager_id,date_hired,salary,department_id) VALUES ('David Hallett', 'Engineer', 90006, to_date('17/04/18', 'DD/MM/YY'), 40000, 2);
INSERT INTO demo.employees (employee_name,job_title,manager_id,date_hired,salary,department_id) VALUES ('Sarah Phelps', 'Manager', 90001, to_date('21/03/15', 'DD/MM/YY'), 45000, 2);
INSERT INTO demo.employees (employee_name,job_title,manager_id,date_hired,salary,department_id) VALUES ('Louise Harper', 'Engineer', 90006, to_date('01/01/13', 'DD/MM/YY'), 47000, 2);
INSERT INTO demo.employees (employee_name,job_title,manager_id,date_hired,salary,department_id) VALUES ('Tina Hart', 'Engineer', 90009, to_date('28/07/14', 'DD/MM/YY'), 45000, 3);
INSERT INTO demo.employees (employee_name,job_title,manager_id,date_hired,salary,department_id) VALUES ('Gus Jones', 'Manager', 90001, to_date('15/05/18', 'DD/MM/YY'), 50000, 3);
INSERT INTO demo.employees (employee_name,job_title,manager_id,date_hired,salary,department_id) VALUES ('Mildred Hall', 'Secretary', 90001, to_date('12/10/96', 'DD/MM/YY'), 35000, 1);
COMMIT;
