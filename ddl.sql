CREATE SEQUENCE demo.seq_dept_id
  START WITH 1
  INCREMENT BY 1
  CACHE 100;

CREATE SEQUENCE demo.seq_emp_id
  START WITH 90001
  INCREMENT BY 1
  CACHE 100;
/
CREATE TABLE demo.department (
         department_id      NUMBER(5) DEFAULT demo.seq_dept_id.nextval PRIMARY KEY,
         department_name    VARCHAR2(50) NOT NULL,
         location        	VARCHAR2(50) NOT NULL);
/
CREATE TABLE demo.employees (
         employee_id      	NUMBER(10) DEFAULT demo.seq_emp_id.nextval PRIMARY KEY,
         employee_name      VARCHAR2(50) NOT NULL,
         job_title        	VARCHAR2(50) NOT NULL,
		 manager_id      	NUMBER(10),
		 date_hired			DATE DEFAULT (sysdate) NOT NULL,
		 salary				NUMBER(10) NOT NULL,
		 department_id		NUMBER(5) REFERENCES demo.department(department_id) NOT NULL
		 			
);	
/